## Poke
**Cr�ation d'un programe en execution avec la CLI**

## php code_generator.php --output_type=json|array --number=10
**The script has to accept different options:**

* output_type: to specify the type of output (json or array),
* number: to specify the number of code to generate.
* The options have to be optional, and the script has to define default value for each options:
* output_type default value is json,
* number default value is 1


## php code_validator.php --code=coupon_code
**The script has to accept 1 option:**

* code: to specify the code to validate.