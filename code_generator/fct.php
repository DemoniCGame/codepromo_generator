<?php

function generateRandomString($length = 6) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function createCouponCode($pdo, $number, $output) {
    for($i = 1; $i <= $number; $i++){
        $code = 'DGTC'.generateRandomString();
        $array[] = $code;
        $req = "insert into coupons(code)
        values('$code');";
        $pdo->exec($req);
    }
    if($output == 'array'){
        return print_r($array);
    }
    return print_r(json_encode($array));
}

function ValidCoupon($pdo, $code){
    $req = "select * from coupons where code = '$code'";
    $res = $pdo->query($req);
    $returnCode = $res->fetch();
    if(empty($returnCode)){
        return print_r('');
    }
    return print_r('1');
}
?>