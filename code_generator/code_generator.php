#!/cli/php
<?php
include("fct.php");

$number = 1;
$output_type = 'json';

$shortopts  = "";
$shortopts .= "output_type::";
$shortopts .= "number::";  
$longopts  = array(
    "output_type::",
    "number::"
);

$options = getopt($shortopts, $longopts);
if(array_key_exists("number", $options)){
    $number = $options["number"];
}
if(array_key_exists("output_type", $options)){
    $output_type = $options["output_type"];
}

/* Modification des paramètres de connexion */
$serveur='mysql:host=localhost'; $bdd='dbname=spliiit';$user='root' ; $mdp='' ;	
/* fin paramètres*/

$pdo = new PDO($serveur.';'.$bdd, $user, $mdp);
$pdo->query("SET CHARACTER SET utf8"); 

createCouponCode($pdo, $number, $output_type);

?>